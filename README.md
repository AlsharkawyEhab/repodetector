# README #

### Repo Detector app ###
* The idea is to create a convenient app for Repostories search using Github

### Support ###
* Swift 4.2
* iOS 11+
* Dependancies is handled by Cocoapods : 1.3.1 (https://cocoapods.org/)
* Xcode 10.1
* Universal (ipad and iphone)
* Portrait and landscape
* Unit Test using XCTest and Mockingjay

### Dependancies ###
* PINRemoteImage: Pinterest, Fast, non-deadlocking parallel image downloader and cache
* Mockingjay: library for stubbing HTTP requests with ease in Swift
* RSLoadingView : animated Activity indicator 
* SwiftyJSON

### Descrption of project ###

#### Section 1: ####
* The app will allow the user to search for GitHub repositories 
* using version 3 of the GitHub API (https://developer.github.com/v3/).
* The app will display a list of repositories. Every item of the list must contain:  
- the avatar image of the repo�s owner
- the name of the repo 
- the description of the repo 
- the number of forks

## High Level Design

![picture](https://i.imgur.com/aws8tgQ.png)

#### Section 2: ####
* Show the details of the repo when clicking a list item
* The detail view will display:
- the name of the repo 
- the count of subscribers
- the list of subscribers


Remaining work:
- Make code little bit clean 
- use better architecture to like MVVM 
- add more code documentation 
- add some UI tests 
- add more unit tests 
