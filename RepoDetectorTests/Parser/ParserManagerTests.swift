//
//  ParserManagerTests.swift
//  RepoDetectorTests
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import XCTest
@testable import RepoDetector

class ParserManagerTests: XCTestCase {
    var parserManager : ParserManager?
    override func setUp() {
        super.setUp()
        self.testInitialization()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testInitialization() {
        let responseRaw: String = "{\"total_count\": 6868, \"incomplete_results\": false, \"items\": [{\"id\": 109423516, \"name\": \"Shadertweak\", \"owner\": { \"id\": 85641, \"avatar_url\": \"https://avatars2.githubusercontent.com/u/85641?v=4\", \"subscriptions_url\": \"https://api.github.com/users/warrenm/subscriptions\" }, \"description\": \"An iPad app that allows you to rapidly prototype fragment shaders in the Metal shading language.\", \"forks_count\": 12 }]}"
        if let data : Data = responseRaw.data(using: .utf8) {
            parserManager = ParserManager(data: data, model: SearchResult(), type: .json)
            XCTAssertNotNil(parserManager, "Parser Manager is nil")
            XCTAssertNotNil(parserManager?.data, "Parser Manager is nil")
            XCTAssertNotNil(parserManager?.model, "Parser Manager is nil")
            XCTAssertNotNil(parserManager?.type, "Parser Manager is nil")
            XCTAssertEqual(parserManager?.type, ParserType.json)
            XCTAssertEqual(parserManager?.data, data)
        }else{
            XCTFail("data is failed")
        }
    }
    
    func testParse(){
        let expectations = expectation(description: "Parser Manager parsing method")
        XCTAssertNotNil(parserManager, "Parser Manager is nil")
        parserManager?.parse(success: { (response) in
            XCTAssertNotNil(response, "response is nil")
            if let searchResult : SearchResult = response as? SearchResult{
                XCTAssertNotNil(searchResult.items, "Items is nil")
                XCTAssertEqual(searchResult.totalCount, 6868)
                XCTAssertEqual(searchResult.incompleteResults, false)
                XCTAssertEqual(searchResult.items.count, 1)
                if let repo : Repository = searchResult.items.first {
                    XCTAssertEqual(repo.id, 109423516)
                    XCTAssertEqual(repo.description, "An iPad app that allows you to rapidly prototype fragment shaders in the Metal shading language.")
                    XCTAssertEqual(repo.forks, 12)
                    XCTAssertEqual(repo.name, "Shadertweak")
                    if let repoOwner: RepositoryOwner = repo.owner {
                        XCTAssertEqual(repoOwner.id, 85641)
                        XCTAssertEqual(repoOwner.avatarUrl, "https://avatars2.githubusercontent.com/u/85641?v=4")
                    }else{
                        XCTFail("repo Owner is not found")
                    }
                }else{
                    XCTFail("Repository is not found")
                }
            } else{
                XCTFail("Search Result is not found")
            }
            expectations.fulfill()
        }, failure: { (error) in
            XCTAssertNotNil(error, "response is nil")
            XCTFail("response as SearchResult is failed with error: \(error)")
            expectations.fulfill()
        })
        waitForExpectations(timeout: 3) { (error) in
            if let error = error {
                XCTFail("testParse function execution time out error: \(error)")
            }
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
}
