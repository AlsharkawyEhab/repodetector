//
//  SearchTableViewController.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import RSLoadingView

private enum TableViewCellIdentifier : String {
    case result = "SearchResultTableViewCellId"
}

/*
 SearchTableViewController is a UITableViewController for showing Search History and results
 */
class SearchTableViewController: UITableViewController {
    //instance of Search Service for handling service layer
    var searchService : SearchService?
    
    //instance of Search result that has response
    lazy var searchResult: SearchResult = SearchResult()
    
    // string of search text
    var query: String = ""
    
    // int of current search page
    var page: Int = 0
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.searchBar.becomeFirstResponder()
    }
    
    func setup(){
        // change font of Search bar
        UITextField.appearance(whenContainedInInstancesOf:[UISearchBar.self]).font = UIFont(name: "Menlo-Regular", size: 15)

        self.title = LocalizationConstants.title.localized
        self.tableView.tableFooterView = UIView(frame: .zero)

        // table view estimated height
        self.tableView.estimatedRowHeight = 120
        self.tableView.rowHeight = UITableView.automaticDimension
        self.searchBar.delegate = self
    }
    
    // MARK: Fetch Data from search Service
    func fetchData() {
        // show activity indicator
        RSLoadingView().showOnKeyWindow()
        
        // increment cuurent page by 1
        self.page += 1
        
        // intialize searchService with current search text and page
        searchService = SearchService(query: self.query, page: String(self.page))
        
        // execute service
        searchService?.execute(success: { (reponse) in
            if let resultResponse : SearchResult = reponse as? SearchResult {
                if resultResponse.items.count > 0 {
                    self.searchResult.incompleteResults = resultResponse.incompleteResults
                    self.searchResult.totalCount = resultResponse.totalCount
                    
                    // add results to cuurent results
                    self.searchResult.items.append(contentsOf: resultResponse.items)
                    
                    // hide activity indicator and reload table view
                    DispatchQueue.main.async {
                        RSLoadingView.hideFromKeyWindow()
                        self.tableView.reloadData()
                    }
                } else if self.searchResult.totalCount == 0, self.page == 1 {
                    self.showErrorMessage(title: "",
                                          body: LocalizationConstants.noRepoErrorViewBody.localized)
                }
            }
        }, failure: { (error) in
            self.showErrorMessage(title: LocalizationConstants.errorViewTitle.localized,
                                  body: error.localizedDescription)
        })
        
    }
    
    // native alert controller to show error messgae
    func showErrorMessage(title: String, body: String) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            RSLoadingView.hideFromKeyWindow()
            let alertController = UIAlertController(title: title.localized, message: body.localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: LocalizationConstants.noRepoErrorViewOk.localized, style: .default)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: - UITableViewDatasource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResult.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SearchResultTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.result.rawValue, for: indexPath) as! SearchResultTableViewCell
        let repo : Repository = self.searchResult.items[indexPath.row]
        cell.setup(repo: repo)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // fetch data when scroll to bottom
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 10
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex,
            self.searchResult.totalCount != 0{
            self.fetchData()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSubscribers", let destination : RepoDetailsViewController = segue.destination as? RepoDetailsViewController{
            if let lastSelectedIndex : IndexPath = self.tableView.indexPathForSelectedRow{
                let repo : Repository = self.searchResult.items[lastSelectedIndex.row]
                destination.urlString = repo.subscribersURL
                destination.title = repo.name
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: - UISearchBarDelegate
extension SearchTableViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchResult.items.removeAll()
        self.tableView.reloadData()
        self.query  = searchBar.text!
        self.page = 0
        self.fetchData()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.page = 0
        return true
    }
}
