//
//  RepoDetailsViewController.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import RSLoadingView

private enum TableViewCellIdentifier : String {
    case subscriber = "RepoDetailsTableViewCellId"
}

class RepoDetailsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subscribersNumberLabel: UILabel!
    // url of subscribers
    var urlString : String?
    
    //instance of Subscibers Service for handling service layer
    var subscriberService : SubscriberService?
    
    //instance of Subscibers List that has response
    lazy var subscibersList: SubscibersList = SubscibersList()
    
    var subscribersDataSource : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    func setup(){
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.tableHeaderView = UIView(frame: .zero)
        if let url = self.urlString, url.count > 0{
            self.fetchData()
        } else{
            self.showErrorMessage(title: "",
                                  body: LocalizationConstants.noSubscribers.localized)
        }
    }
    
    // MARK: Fetch Data from search Service
    func fetchData() {
        // show activity indicator
        RSLoadingView().showOnKeyWindow()
        
        // intialize searchService with current search text and page
        self.subscriberService = SubscriberService(urlString: self.urlString ?? "")
        
        // execute service
        self.subscriberService?.execute(success: { (reponse) in
            
            if let subscibersList : SubscibersList = reponse as? SubscibersList {
                self.subscibersList = subscibersList
                DispatchQueue.main.async {
                    self.subscribersNumberLabel.text = "Number of Subscibers: \(self.subscibersList.items.count)"
                }
                if self.subscibersList.items.count > 0 {
                    
                    // hide activity indicator and reload table view
                    DispatchQueue.main.async {
                        RSLoadingView.hideFromKeyWindow()
                        self.tableView.reloadData()
                    }
                } else{
                    self.showErrorMessage(title: "",
                                          body: LocalizationConstants.noSubscribers.localized)
                }
            }
        }, failure: { (error) in
            self.showErrorMessage(title: LocalizationConstants.errorViewTitle.localized,
                                  body: error.localizedDescription)
        })
        
    }
    
    // native alert controller to show error messgae
    func showErrorMessage(title: String, body: String) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            RSLoadingView.hideFromKeyWindow()
            let alertController = UIAlertController(title: title.localized, message: body.localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: LocalizationConstants.noRepoErrorViewOk.localized, style: .default)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension RepoDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.subscibersList.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RepoDetailsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.subscriber.rawValue, for: indexPath) as! RepoDetailsTableViewCell
        let subscriber : Subscriber = self.subscibersList.items[indexPath.row]
        cell.setup(subscriber: subscriber)
        return cell
    }
}

