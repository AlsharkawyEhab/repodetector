//
//  Service.swift
//  RepoDetector
//
//  Created by Ehab on 09/01/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
/*
 Base protocol of all Services
 */
protocol Service {
    // Success callback with Any response
    typealias success = (Any?) -> Void
    
    // Failure callback with Any response
    typealias failure = (NSError) -> Void
    
    // Service Request with Any response like SearchRequest
    var serviceRequest : Request { get set}
    
    var networkManager : NetworkManager{ get set}
    
    /*
     execute function
     @callback success with Any type or failure of NSError type
     */
    func execute(success:@escaping success, failure:  @escaping failure)
}
