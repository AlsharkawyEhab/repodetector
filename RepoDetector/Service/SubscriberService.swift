//
//  SubscriberService.swift
//  RepoDetector
//
//  Created by Ehab on 09/01/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation

class SubscriberService : Service {
    var serviceRequest: Request
    var networkManager : NetworkManager
    var subscriberURLString : String = ""
    var parserManger : ParserManager?
    var subscriberList : SubscibersList = SubscibersList()
    
    /*
     Intialization
     @param query type: String, like "Metal"
     @param page type: String, like "2"
     */
    init(urlString : String) {
        self.subscriberURLString = urlString
        self.serviceRequest = SubscriberRequest(urlString: urlString)
        self.networkManager = NetworkManager(request: self.serviceRequest.request)
    }
    
    /*
     execute function
     @callback success with Any type or failure of NSError type
     */
    func execute(success:@escaping success, failure:  @escaping failure) {
        networkManager.execute(success: { (response) in
            if let responseData : Data = response {
                self.parserManger = ParserManager(data: responseData, model: self.subscriberList, type: .json)
                self.parserManger?.parse(success: success, failure: failure)
            } else {
                success(nil)
            }
        }, failure: failure)
    }
}
