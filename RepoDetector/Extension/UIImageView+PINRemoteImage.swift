//
//  UIImageView+PINRemoteImage.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import PINRemoteImage

/*
 Extension for UIImageView is responsable for set image with URL
 */
extension UIImageView {
    
    func setImageWithURL(urlString: String?){
        self.setImageWithURLUsingPinCache(urlString: urlString ?? "")
    }
    
    // set image using PINRemoteImage
    private func setImageWithURLUsingPinCache(urlString: String){
        self.pin_updateWithProgress = true
        self.pin_setImage(from: URL(string: urlString)!, placeholderImage: UIImage(named: "Logo"))
    }
    
}
