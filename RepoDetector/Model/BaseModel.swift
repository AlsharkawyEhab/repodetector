//
//  BaseModel.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 base protocol of all models
 */

protocol BaseModel {
    
    // success callback with BaseModel response
    typealias success = (BaseModel) -> Void
    
    // failure callback with NSError response
    typealias failure = (NSError) -> Void
    
    /*
     parse function
     @param json of JSON type
     @return parsed json of BaseModel type
     */
    func parse(json: JSON) -> BaseModel
}
