//
//  ModelKeysConstants.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation

//MARK: - Search Result Model
enum SearchResultModelConstants : String {
    case totalCount = "total_count"
    case incompleteResults = "incomplete_results"
    case items = "items"
}

enum RepositoryModelConstants: String {
    case id = "id"
    case name = "name"
    case description = "description"
    case forks = "forks_count"
    case owner = "owner"
    case subscribersURL = "subscribers_url"
}

enum RepositoryOwnerModelConstants: String {
    case id = "id"
    case avatarUrl = "avatar_url"
}

enum SubscriberModelConstants: String {
    case id = "id"
    case avatarUrl = "avatar_url"
    case name = "login"
}
