//
//  Subscriber.swift
//  RepoDetector
//
//  Created by Ehab on 09/01/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation

class Subscriber : BaseModel {
    var id: Int?
    var avatarUrl: String?
    var name: String?
}
