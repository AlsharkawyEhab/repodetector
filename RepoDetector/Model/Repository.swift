//
//  repository.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation

class Repository : BaseModel {
    var id: Int?
    var name : String?
    var description: String?
    var forks: Int?
    var owner: RepositoryOwner?
    var subscribersURL : String?
}
