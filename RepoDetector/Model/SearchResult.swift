//
//  SearchResult.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation

class SearchResult : BaseModel {
    var totalCount: Int?
    var incompleteResults: Bool?
    var items: [Repository] = [Repository]()
}
