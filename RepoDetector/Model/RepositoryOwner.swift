//
//  RepositoryOwner.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation

class RepositoryOwner : BaseModel {
    var id: Int?
    var avatarUrl: String?
}

