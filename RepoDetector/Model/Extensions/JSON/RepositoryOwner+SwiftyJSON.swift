//
//  RepositoryOwner+SwiftyJSON.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 Extension for RepositoryOwner + SwiftyJSON, is responsable for parsing using json
 */
extension RepositoryOwner {
    func parse(json: JSON) -> BaseModel {
        self.id = json[RepositoryOwnerModelConstants.id.rawValue].int
        self.avatarUrl = json[RepositoryOwnerModelConstants.avatarUrl.rawValue].string
        return self
    }
}
