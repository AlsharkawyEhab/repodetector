//
//  SubscibersList+SwiftJSON.swift
//  RepoDetector
//
//  Created by Ehab on 09/01/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 Extension for Subscriber + SwiftyJSON, is responsable for parsing using json
 */
extension SubscibersList {
    func parse(json: JSON) -> BaseModel {
        if let list : Array = json.array{
            for item in list {
                if let subscriber : Subscriber = Subscriber().parse(json: item) as? Subscriber{
                    self.items.append(subscriber)
                }
            }
        }
        return self
    }
}
