//
//  SearchResult+JSON.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 Extension for SearchResult + SwiftyJSON, is responsable for parsing using json
 */
extension SearchResult {
    func parse(json: JSON) -> BaseModel {
        self.incompleteResults = json[SearchResultModelConstants.incompleteResults.rawValue].bool
        self.totalCount = json[SearchResultModelConstants.totalCount.rawValue].int
        if let resultsArray : Array = json[SearchResultModelConstants.items.rawValue].array{
            for item in resultsArray {
                if let repo : Repository = Repository().parse(json: item) as? Repository{
                    self.items.append(repo)
                }
            }
        }
        return self
    }
}
