//
//  Repository+SwiftyJSON.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 Extension for Repository + SwiftyJSON, is responsable for parsing using json
 */
extension Repository {
    func parse(json: JSON) -> BaseModel {
        self.name = json[RepositoryModelConstants.name.rawValue].string
        self.forks = json[RepositoryModelConstants.forks.rawValue].int
        self.description = json[RepositoryModelConstants.description.rawValue].string
        self.id = json[RepositoryModelConstants.id.rawValue].int
        self.owner = RepositoryOwner().parse(json: json[RepositoryModelConstants.owner.rawValue]) as? RepositoryOwner
        self.subscribersURL = json[RepositoryModelConstants.subscribersURL.rawValue].string
        return self
    }
}
