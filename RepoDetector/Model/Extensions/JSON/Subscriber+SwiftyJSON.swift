//
//  Subscriber+SwiftyJSON.swift
//  RepoDetector
//
//  Created by Ehab on 09/01/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 Extension for Subscriber + SwiftyJSON, is responsable for parsing using json
 */
extension Subscriber {
    func parse(json: JSON) -> BaseModel {
        self.id = json[SubscriberModelConstants.id.rawValue].int
        self.avatarUrl = json[SubscriberModelConstants.avatarUrl.rawValue].string
        self.name = json[SubscriberModelConstants.name.rawValue].string
        return self
    }
}
