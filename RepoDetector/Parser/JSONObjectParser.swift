//
//  JSONObjectParser.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright © 2019 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 JSONObjectParser is responsable for handling parsing Json operations
 */
class JSONObjectParser: ParserProtocol {
    
    /*
     parse function
     @param data type: Data
     @param model type: BaseModel
     @callback success with Any type or failure of NSError type
     */
    func parse(data: Data, model: BaseModel, success:@escaping success, failure:  @escaping failure) {
        if let json = try? JSON(data: data) {
            let parsedObject : BaseModel = model.parse(json: json)
            success(parsedObject)
        } else {
            failure(NSError.init(domain: "Parser Error", code: -31616, userInfo: nil))
        }
    }
}
