//
//  NetworkManager.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright 2019 Github. All rights reserved.
//

import Foundation

/*
 Network Manager for handling Network operations
 */

struct NetworkManager {
    
    typealias NetworkSuccessClosure = (Data?) -> Void
    typealias NetworkErrorClosure = (NSError) -> Void
    let concurrentQueue = DispatchQueue(label: "Network Manager queue", attributes: .concurrent)
    
    let session = URLSession.shared
    var request : URLRequest
    /*
     initialization
     @param URLRequest request
     */
    init(request : URLRequest) {
        self.request = request
        session.configuration.timeoutIntervalForRequest = request.timeoutInterval
    }
    
    /*
     execute is a function to execute a request using `URLSession`
     @param callback success: return Objects of type Data. `nil` by default.
     @param callback failure: return NSError Object.
     */
    
    func execute(success:@escaping NetworkSuccessClosure, failure:  @escaping NetworkErrorClosure) {
        concurrentQueue.sync {
            
            let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                if let errorResponse : NSError = error as NSError? {
                    failure(errorResponse.generateNetworkError())
                } else if let dataResponse : Data = data {
                    success(dataResponse)
                } else{
                    let error : NSError = NSError(domain: (self.request.url?.absoluteString) ?? "", code: NetworkErrorCode.emptyResponse.rawValue, userInfo: nil)
                    failure(error.generateNetworkError())
                }
            })
            
            dataTask.resume()
        }
    }
    
    /*
     cancelAllTasks is a function to cancell all current requests
     */
    func cancelAllTasks() {
        session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
}
