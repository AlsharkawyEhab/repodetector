//
//  SearchRequest.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright 2019 Github. All rights reserved.
//

import Foundation

/*
 SearchRequestParamters enum is responsable for having Search request constants
 */
enum SearchRequestParamters : String {
    case pathKey = "/search/repositories?"
    case queryKey = "q="
    case perPageKey = "&per_page="
    case pageKey = "&page="
}

/*
 SearchRequest is responsable for Search request model
 */
struct SearchRequest : Request {
    /*
     query String, search text eg. "Metal"
     */
    var query : String
    /*
     page String, current page of search list eg. "3"
     */
    var page : String
    
   var perPage : String {
        get{
            return "50"
        }
    }

    /*
     Intialization
     @param query String, search text eg. "Metal"
     @param page String, current page of search list eg. "3"
     */
    init(query : String, page : String) {
        self.query = query
        self.page = page
    }
    /*
     getSearchURLRequestPath , calculate urlPath
     url path is compination of baseURL + apiKeyValue + queryKey + pageKey
     */
    func getURLPath(serviceConfigration : ServiceConfigration) -> String {
        return serviceConfigration.baseURL +  SearchRequestParamters.pathKey.rawValue
            + SearchRequestParamters.queryKey.rawValue + self.query
            + SearchRequestParamters.pageKey.rawValue + self.page
            + SearchRequestParamters.perPageKey.rawValue + self.perPage
    }
    /*
     request : URLRequest, is readonly variable that calculate urlPath
     */
    var request : URLRequest {
        get{
            let serviceConfigration : ServiceConfigration = StateManager().serviceConfigration
            
            let urlPath : String = self.getURLPath(serviceConfigration:serviceConfigration)
            
            if let encodedURL : String = urlPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let url : URL = URL(string: encodedURL) {
                var urlRequest : URLRequest = URLRequest(url: url)
                urlRequest.httpMethod = "GET"
                urlRequest.timeoutInterval = 10
                urlRequest.cachePolicy = .useProtocolCachePolicy
                return urlRequest
            }
            return URLRequest(url: URL(string: "")!)
        }
    }
}
