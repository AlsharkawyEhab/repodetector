//
//  SubscriberRequest.swift
//  RepoDetector
//
//  Created by Ehab on 09/01/19.
//  Copyright 2019 Github. All rights reserved.
//

import Foundation

/*
 SearchRequest is responsable for Search request model
 */
struct SubscriberRequest : Request {
    /*
     urlString String, url of subscribers
     */
    var urlString : String
    
    /*
     Intialization
     @param urlString String, url of subscribers
     */
    init(urlString : String) {
        self.urlString = urlString
    }
    
    /*
     getSearchURLRequestPath , calculate urlPath
     */
    private func getURLPath() -> String {
        return self.urlString
    }
    /*
     request : URLRequest, is readonly variable that calculate urlPath
     */
    var request : URLRequest {
        get{
            let urlPath : String = self.urlString
            
            if let url : URL = URL(string: urlPath) {
                var urlRequest : URLRequest = URLRequest(url: url)
                urlRequest.httpMethod = "GET"
                urlRequest.timeoutInterval = 10
                urlRequest.cachePolicy = .useProtocolCachePolicy
                return urlRequest
            }
            return URLRequest(url: URL(string: "Failed")!)
        }
    }
}
