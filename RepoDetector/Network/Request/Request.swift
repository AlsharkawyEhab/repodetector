//
//  Request.swift
//  RepoDetector
//
//  Created by Ehab on 01/09/19.
//  Copyright 2019 Github. All rights reserved.
//

import Foundation

protocol Request {
    var request : URLRequest { get }
}
